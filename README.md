# Welcome to the wiki for WONKA: Objective novel complex analysis

## Try online:

1. Go ahead and [try the functionality](http://wonka.sgc.ox.ac.uk/WONKA/) online using openly available data.

## Personal use (windows):

1. Run the windows installer. [Install here!](https://bitbucket.org/abradley/wonka/downloads/Setup.exe) 

2. Launch the GUI!

More detailed instructions available [here](https://bitbucket.org/abradley/wonka/wiki/Setting%20up%20on%20Windows)


## Personal use (mac):
### RECOMMENDED
We'd recommend using custom built Docker images.

We have provided a Docker image for setting up a WONKA [development environment](https://bitbucket.org/abradley/wonka/wiki/Using%20Docker%20to%20run%20WONKA%20on%20MacOSX) and a server for WONKA

### NOT RECOMMENDED
Below are install [instructions for MacOSX](https://bitbucket.org/abradley/wonka/wiki/Mac%20OSX) - however there many demons in the install - we would HIGHLY recommend that you use Docker.

## Personal use (linux):

###RECOMMENDD
Again we'd recommend [Docker](https://bitbucket.org/abradley/wonka/wiki/Linux)


### Hardcore NOT RECOMMENDED
Feel free to install the [dependencies](https://bitbucket.org/abradley/wonka/wiki/Dependencies) and use the source code natively.

Specific instructions on setting this up for Ubuntu 12.04 LTS are shown [here](https://bitbucket.org/abradley/wonka/wiki/Ubuntu). 

## Loading data and processing:

Loading data can be done using the command line tool

Instructions [here](https://bitbucket.org/abradley/wonka/wiki/LoadingData)

## Setting up as a server:

Here we have instructions for setting up an OOMMPPAA server on [Ubuntu](https://bitbucket.org/abradley/oommppaa/wiki/Ubuntu%20server). This setup makes it possible to give site wide access to the tool and would be best if a group of people want to use the same data.


## Disqus integration

The WONKA page is integrated with Disqus discussion pages.

You will need to configure your own setup for your own site. Otherwise ALL comments you make will be visible by me and all other users of WONKA.

This means:

1. Set up an account for your site using [Disqus](https://disqus.com/)

2. Get your DISQUS_SECRET_KEY; DISQUS_PUBLIC_KEY; and DIQSUS_SHORT_NAME

3. Change the values in personalsettings.py to update these values /CHOC/src/WebApp/WebApp/personalsettings.py in the DOCKER images

4. Restart the server


# FAQS

**Why is the ActiveICM window frozen or hovering over all the web pages?
**

I'm afraid this is a known bug with the plugin that we are using. You will have to close the window down.

**Why doesn't it work?**

I'm sorry! E-mail the mailing list and I will get back to you as soon as possible.
wonka.help@gmail.com

**Is a mac version available?**

There are plans to make a Mac installer.
At the moment you can download the source code and install the required dependencies, or use Docker.